﻿namespace FeaturesGeneratorIA {
    partial class Form1 {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent() {
            this.label1 = new System.Windows.Forms.Label();
            this.CaminhoDataset = new System.Windows.Forms.TextBox();
            this.AbrirArquivoDataSet = new System.Windows.Forms.Button();
            this.AbrirArquivoSentiWordNet = new System.Windows.Forms.Button();
            this.CaminhoSentiWordNet = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.EscolherPasta = new System.Windows.Forms.Button();
            this.CaminhoDestino = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.GerarFeatures = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Caminho Dataset";
            // 
            // CaminhoDataset
            // 
            this.CaminhoDataset.Location = new System.Drawing.Point(16, 29);
            this.CaminhoDataset.Name = "CaminhoDataset";
            this.CaminhoDataset.Size = new System.Drawing.Size(156, 20);
            this.CaminhoDataset.TabIndex = 1;
            this.CaminhoDataset.Text = "C:\\Users\\eduar\\Dropbox\\Semestre\\IA\\Trabalho NP2\\bases\\IMDB Clean.arff";
            // 
            // AbrirArquivoDataSet
            // 
            this.AbrirArquivoDataSet.Location = new System.Drawing.Point(188, 27);
            this.AbrirArquivoDataSet.Name = "AbrirArquivoDataSet";
            this.AbrirArquivoDataSet.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.AbrirArquivoDataSet.Size = new System.Drawing.Size(75, 23);
            this.AbrirArquivoDataSet.TabIndex = 2;
            this.AbrirArquivoDataSet.Text = "Abrir Arquivo";
            this.AbrirArquivoDataSet.UseVisualStyleBackColor = true;
            this.AbrirArquivoDataSet.Click += new System.EventHandler(this.AbrirArquivoDataSet_Click);
            // 
            // AbrirArquivoSentiWordNet
            // 
            this.AbrirArquivoSentiWordNet.Location = new System.Drawing.Point(188, 73);
            this.AbrirArquivoSentiWordNet.Name = "AbrirArquivoSentiWordNet";
            this.AbrirArquivoSentiWordNet.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.AbrirArquivoSentiWordNet.Size = new System.Drawing.Size(75, 23);
            this.AbrirArquivoSentiWordNet.TabIndex = 5;
            this.AbrirArquivoSentiWordNet.Text = "Abrir Arquivo";
            this.AbrirArquivoSentiWordNet.UseVisualStyleBackColor = true;
            this.AbrirArquivoSentiWordNet.Click += new System.EventHandler(this.AbrirArquivoSentiWordNet_Click);
            // 
            // CaminhoSentiWordNet
            // 
            this.CaminhoSentiWordNet.Location = new System.Drawing.Point(16, 75);
            this.CaminhoSentiWordNet.Name = "CaminhoSentiWordNet";
            this.CaminhoSentiWordNet.Size = new System.Drawing.Size(156, 20);
            this.CaminhoSentiWordNet.TabIndex = 4;
            this.CaminhoSentiWordNet.Text = "C:\\Users\\eduar\\Dropbox\\Semestre\\IA\\Trabalho NP2\\bases\\SentiWordNet_3.0.0_20130122" +
    " Clean.txt";
            this.CaminhoSentiWordNet.TextChanged += new System.EventHandler(this.CaminhoSentiWordNet_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Caminho SentiWordNet";
            // 
            // EscolherPasta
            // 
            this.EscolherPasta.Location = new System.Drawing.Point(178, 121);
            this.EscolherPasta.Name = "EscolherPasta";
            this.EscolherPasta.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.EscolherPasta.Size = new System.Drawing.Size(100, 23);
            this.EscolherPasta.TabIndex = 8;
            this.EscolherPasta.Text = "Escolher Pasta";
            this.EscolherPasta.UseVisualStyleBackColor = true;
            this.EscolherPasta.Click += new System.EventHandler(this.EscolherPasta_Click);
            // 
            // CaminhoDestino
            // 
            this.CaminhoDestino.Location = new System.Drawing.Point(16, 123);
            this.CaminhoDestino.Name = "CaminhoDestino";
            this.CaminhoDestino.Size = new System.Drawing.Size(156, 20);
            this.CaminhoDestino.TabIndex = 7;
            this.CaminhoDestino.Text = "C:\\Users\\eduar\\Dropbox\\Semestre\\IA\\Trabalho NP2\\GeneratedFeatures";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 107);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Caminho Destino";
            // 
            // GerarFeatures
            // 
            this.GerarFeatures.Location = new System.Drawing.Point(163, 226);
            this.GerarFeatures.Name = "GerarFeatures";
            this.GerarFeatures.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.GerarFeatures.Size = new System.Drawing.Size(100, 23);
            this.GerarFeatures.TabIndex = 9;
            this.GerarFeatures.Text = "Gerar";
            this.GerarFeatures.UseVisualStyleBackColor = true;
            this.GerarFeatures.Click += new System.EventHandler(this.GerarFeatures_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.GerarFeatures);
            this.Controls.Add(this.EscolherPasta);
            this.Controls.Add(this.CaminhoDestino);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AbrirArquivoSentiWordNet);
            this.Controls.Add(this.CaminhoSentiWordNet);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.AbrirArquivoDataSet);
            this.Controls.Add(this.CaminhoDataset);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Features Generator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CaminhoDataset;
        private System.Windows.Forms.Button AbrirArquivoDataSet;
        private System.Windows.Forms.Button AbrirArquivoSentiWordNet;
        private System.Windows.Forms.TextBox CaminhoSentiWordNet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button EscolherPasta;
        private System.Windows.Forms.TextBox CaminhoDestino;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button GerarFeatures;
    }
}

