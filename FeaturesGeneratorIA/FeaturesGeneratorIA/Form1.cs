﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Collections;
using System.Windows.Forms;

namespace FeaturesGeneratorIA
{
    public partial class Form1 : Form
    {

        string SelectedDatasetFilePath = @"C:\Users\eduar\Desktop\Unifor\IA\bases\IMDB Clean.arff";
        string SelectedSentiWordFilePath = @"C:\Users\eduar\Desktop\Unifor\IA\bases\SentiWordNet_3.0.0_20130122 Clean.txt";
        string SelectedDestinyFolderPath = @"C:\Users\eduar\Desktop\Unifor\IA\GeneratedFeatures";


        public Form1()
        {
            InitializeComponent();
        }

        private void AbrirArquivoDataSet_Click(object sender, EventArgs e)
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "All Files (*.*)|*.*";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = false;

            if (choofdlog.ShowDialog() == DialogResult.OK)
                SelectedDatasetFilePath = choofdlog.FileName;
            else
                SelectedDatasetFilePath = string.Empty;

            CaminhoDataset.Text = SelectedDatasetFilePath;

        }

        private void AbrirArquivoSentiWordNet_Click(object sender, EventArgs e)
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
            choofdlog.Filter = "All Files (*.*)|*.*";
            choofdlog.FilterIndex = 1;
            choofdlog.Multiselect = false;

            if (choofdlog.ShowDialog() == DialogResult.OK)
                SelectedSentiWordFilePath = choofdlog.FileName;
            else
                SelectedSentiWordFilePath = string.Empty;

            CaminhoSentiWordNet.Text = SelectedSentiWordFilePath;
        }

        private void EscolherPasta_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            //fbd.Description = "Custom Description"; //not mandatory

            if (fbd.ShowDialog() == DialogResult.OK)
                SelectedDestinyFolderPath = fbd.SelectedPath;
            else
                SelectedDestinyFolderPath = string.Empty;


            CaminhoDestino.Text = SelectedDestinyFolderPath;
        }

        private void GerarFeatures_Click(object sender, EventArgs e)
        {

            //Cria o arquivo de novo dataset
            string NewDatasetFilePath = SelectedDestinyFolderPath + @"\NewDataset.txt";
            StreamWriter sw = File.CreateText(NewDatasetFilePath);
            sw.Close();

            //SentiWordNet
            SentiWordNet Sentiwordnet = new SentiWordNet(SelectedSentiWordFilePath);

            //IMDB dataset
            string Line;
            ArrayList LineSplitArray = new ArrayList();

            //Features
            int WordCount = 0; //número de palavras
            double NounScoreSum = 0; //soma da polaridade dos substantivos
            double AdjectiveScoreSum = 0; //soma da polaridade dos adjetivos
            double AdverbScoreSum = 0; //soma da polaridade dos verbos
            double VerbScoreSum = 0;//soma da polaridade dos advérbios
            int WordMissCount = 0; //quantidade de palavras não encontradas
            string PosOrNeg = string.Empty; //se o review foi negativo ou positivo

            string NewDataSetLine = string.Empty; //Nova linha do novo dataset
            List<string> NewDataSetAllLines = new List<string>();

            using (StreamReader sr = new StreamReader(SelectedDatasetFilePath))
            {
                while (sr.Peek() >= 0)
                {
                    WordCount = 0;
                    NounScoreSum = 0; //n
                    AdjectiveScoreSum = 0; //a
                    AdverbScoreSum = 0; //r
                    VerbScoreSum = 0; //v
                    WordMissCount = 0;
                    PosOrNeg = string.Empty;
                    LineSplitArray.Clear();

                    Line = sr.ReadLine().ToLower();
                    PosOrNeg = Line.Substring(Line.Length - 3); // salva se o review foi neg ou pos
                    Line = Line.Substring(1, Line.Length - 6); //remove o inicio ' e final ',neg / ',pos
                    Line = Line.Replace('"', ' ');
                    Line = Line.Replace('.', ' ');
                    Line = Line.Replace(',', ' ');
                    Line = Line.Replace('(', ' ');
                    Line = Line.Replace(')', ' ');
                    Line = Line.Replace('!', ' ');
                    Line = Line.Replace('?', ' ');
                    Line = Line.Replace(';', ' ');
                    Line = Line.Replace(':', ' ');
                    Line = Line.Replace('&', ' ');
                    LineSplitArray.AddRange(Line.Split(' '));

                    while (LineSplitArray.Contains(""))
                    {
                        LineSplitArray.Remove("");
                    }

                    WordCount = LineSplitArray.Count; //Getting the amount of words

                    foreach (string Word in LineSplitArray)
                    {
                        if (Sentiwordnet.CheckExists(Word, "n")) //if it is noun
                        {
                            NounScoreSum += Math.Round(Sentiwordnet.Extract(Word, "n"), 2);
                        }
                        else if (Sentiwordnet.CheckExists(Word, "a")) //if it is adjective
                        {
                            AdjectiveScoreSum += Math.Round(Sentiwordnet.Extract(Word, "a"), 2);
                        }
                        else if (Sentiwordnet.CheckExists(Word, "r")) //if it is adverb
                        {
                            AdverbScoreSum += Math.Round(Sentiwordnet.Extract(Word, "r"), 2);
                        }
                        else if (Sentiwordnet.CheckExists(Word, "v")) //if it is verb
                        {
                            VerbScoreSum += Math.Round(Sentiwordnet.Extract(Word, "v"), 2);
                        }
                        else
                        {
                            WordMissCount++;
                        }
                    }

                    string WordCountStr = WordCount.ToString().Replace(',', '.');
                    string NounScoreSumStr = NounScoreSum.ToString().Replace(',', '.');
                    string AdjectiveScoreSumStr = AdjectiveScoreSum.ToString().Replace(',', '.');
                    string AdverbScoreSumStr = AdverbScoreSum.ToString().Replace(',', '.');
                    string VerbScoreSumStr = VerbScoreSum.ToString().Replace(',', '.');
                    string WordMissCountStr = WordMissCount.ToString().Replace(',', '.');

                    NewDataSetLine = WordCountStr + "," + NounScoreSumStr + "," + AdjectiveScoreSumStr + "," +
                        AdverbScoreSumStr + "," + VerbScoreSumStr + "," + WordMissCountStr + "," + PosOrNeg;

                    NewDataSetAllLines.Add(NewDataSetLine);
                }

            }

            string[] NewDataSetAllLinesArray = NewDataSetAllLines.ToArray();

            File.WriteAllLines(NewDatasetFilePath, NewDataSetAllLinesArray);

        }

        private void CaminhoSentiWordNet_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
