﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FeaturesGeneratorIA
{
    class SentiWordNet
    {
        private Dictionary<string, double> Dict;

        public SentiWordNet(String pathToSWN)
        {
            // This is our main dictionary representation
            Dict = new Dictionary<string, double>();

            // From String to list of doubles.
            Dictionary<string, Dictionary<int, double>> TempDictionary = new Dictionary<string, Dictionary<int, double>>();

            using (StreamReader sr = new StreamReader(pathToSWN))
            {
                string[] Data;
                string WordTypeMarker;
                string Line;
                ArrayList LineSplitArray = new ArrayList();

                while (sr.Peek() >= 0)
                {
                    Line = sr.ReadLine().ToLower();
                    Data = Line.Split('\t');
                    WordTypeMarker = Data[0];

                    // Calculate synset score as score = PosS - NegS
                    float posS = float.Parse(Data[2].Replace('.', ','));
                    float negS = float.Parse(Data[3].Replace('.', ','));
                    double SynsetScore = posS - negS;

                    // Get all Synset terms
                    string[] SynTermsSplit = Data[4].Split(' ');

                    foreach(string SynTermCurrent in SynTermsSplit)
                    {
                        // Get synterm and synterm rank
                        string[] SynTermAndRank = SynTermCurrent.Split('#');
                        string SynTerm = SynTermAndRank[0] + "#" + WordTypeMarker; //able#a

                        int SynTermRank = int.Parse(SynTermAndRank[1]); // #1 or #2
                                                                        // What we get here is a map of the type:
                                                                        // term -> {score of synset#1, score of synset#2...}

                        // Add map to term if it doesn't have one
                        if (!TempDictionary.ContainsKey(SynTerm))
                        {
                            TempDictionary.Add(SynTerm, new Dictionary<int, double>());
                        }

                        // Add synset link to synterm
                        TempDictionary[SynTerm].Add(SynTermRank, SynsetScore);
                    }
                }
            }

            // Go through all the terms

            foreach (KeyValuePair<string, Dictionary<int, double>> entry in TempDictionary)
            {
                String Word = entry.Key;

                if (Dict.ContainsKey(Word))
                {
                    continue;
                }

                Dictionary<int, double> SynSetScoreMap = entry.Value;

                // Calculate weighted average. Weigh the synsets according to
                // their rank.
                // Score= 1/2*first + 1/3*second + 1/4*third ..... etc.
                // Sum = 1/1 + 1/2 + 1/3 ...
                double Score = 0.0;
                double Sum = 0.0;

                foreach (KeyValuePair<int, double> SetScore in SynSetScoreMap)
                {
                    Score += SetScore.Value / SetScore.Key;
                    Sum += 1.0 / SetScore.Key;
                }

                Score /= Sum;

                if (!Dict.ContainsKey(Word))
                {
                    Dict.Add(Word, Score);
                }
            }
            Console.Write("");
        }

        public double Extract(string Word, string Pos)
        {
            double value = 0;

            if(Dict.TryGetValue(Word + "#" + Pos, out value))
            {
                //Console.WriteLine(value);
                return value;
            }

            //Console.WriteLine(value);
            return 0; //get(word + "#" + pos (a,r,n,v) );
        }

        public bool CheckExists(string Word, string Pos)
        {
            if (!Dict.ContainsKey(Word + "#" + Pos))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}
